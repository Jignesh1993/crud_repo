var jwt = require("jsonwebtoken");

const jwtverify = (token) => {
  return jwt.verify(token, `${process.env.JWTsecretkey}`, function (
    err,
    decoded
  ) {
    if (!err) {
      return true;
    }
    return false;
  });
};

module.exports = jwtverify;

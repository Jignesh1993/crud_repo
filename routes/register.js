var express = require("express");
var router = express.Router();
var RegisterSchema = require("../models/registerModel");

/* GET home page. */
router.post("/register", function (req, res, next) {
  const data = JSON.parse(JSON.stringify(req.body));
  const registerModel = new RegisterSchema(data);

  registerModel.save(function (err, register) {
    if (err){
      res.json(err);
    } 
    res.json(register);
  });
});

module.exports = router;

var express = require("express");
var router = express.Router();
var register = require("../models/registerModel");
const jwtverify = require("../routes/jwtverify");

router.get("/view", (req, res) => {
  const token = req.headers.authorization.split(" ")[1];
  const result = jwtverify(token);
  if (result) {
    register.find({}, function (err, result) {
      if (result) {
        res.json(result);
      }
    });
  }
  if (!result) {
    res.json(null);
  }
});

module.exports = router;

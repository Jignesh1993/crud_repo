var express = require('express');
var router = express.Router();
require("../config/connect");
var registerRouter = require("./register");
var loginRouter = require("./login");
var viewRouter = require("./view");
var updateRouter = require("./update");
var deleteRouter = require("./delete");

/* GET home page. */
router.get('/', function(req, res, next) {
 res.send('Index');
});

router.post("/register",registerRouter);
router.get("/login",loginRouter);
router.get("/view",viewRouter);
router.put("/update",updateRouter);
router.delete("/delete",deleteRouter);

module.exports = router;

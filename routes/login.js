var express = require("express");
var router = express.Router();
var jwt = require('jsonwebtoken');
var register = require("../models/registerModel");

router.get("/login", (req, res) => {
  const data = JSON.parse(JSON.stringify(req.body));
  const username = data.name;
  const password = data.password;
  let token = '';

  register.findOne({ name: username, password: password }, function (
    err,
    result
  ) {
    if (result) {
      token = jwt.sign({ name: result }, `${process.env.JWTsecretkey}`);
      res.json({token: token});
    } else {
      res.json(null);
    }
  });
});

module.exports = router;

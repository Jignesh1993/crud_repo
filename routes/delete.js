var express = require("express");
var router = express.Router();
var jwt = require("jsonwebtoken");
var register = require("../models/registerModel");
const jwtverify = require("../routes/jwtverify");
var mongoose = require("mongoose");

router.delete("/delete", (req, res) => {
  const token = req.headers.authorization.split(" ")[1];
  const id = mongoose.Types.ObjectId(req.query.id);
  const result = jwtverify(token);
  if (result) {
    register.deleteOne({ _id: id }, (err, result) => {
      if (result) {
        res.json(result);
      }
      if (err) {
        res.json(err);
      }
    });
  }
  if (!result) {
    res.json(null);
  }
});

module.exports = router;

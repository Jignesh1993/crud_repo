// Define schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RegisterModelSchema = new Schema({
  name: String,
  email: String,
  mobile: String,
  age: { type: Number, min: 18, max: 65, required: true },
  city: String,
  password: String
});

// Compile model from schema
var RegisterModel = mongoose.model('RegisterModel', RegisterModelSchema, 'registration' );
module.exports = RegisterModel;